package com.gol.suplier.service.impl;

import com.gol.suplier.domain.City;
import com.gol.suplier.service.CityService;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
public class CityServiceInMemoryImpl implements CityService {

    @Override
    public List<City> getAllCities() {

        List<City> cities = new ArrayList<>();
        cities.add(new City("001","Warszawa", BigInteger.valueOf(1_750_000L)));
        cities.add(new City("002","Wrocław", BigInteger.valueOf(628_000L)));
        cities.add(new City("003","Łódź", BigInteger.valueOf(701_000L)));
        cities.add(new City("004","Gdańsk", BigInteger.valueOf(460_000L)));

        return cities;
    }
}
