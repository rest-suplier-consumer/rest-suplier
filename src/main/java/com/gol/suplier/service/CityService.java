package com.gol.suplier.service;

import com.gol.suplier.domain.City;

import java.util.List;

public interface CityService {

    List<City> getAllCities();

}
