package com.gol.suplier.rest;

import com.gol.suplier.domain.City;
import com.gol.suplier.domain.CityListDTO;
import com.gol.suplier.service.CityService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j
@RestController
public class SuplierApi {

    @Autowired
    private CityService cityService;

    @GetMapping("/")
    public String welcomePage() {
        return "Hello, I'm working";
    }

    @GetMapping("/cityList")
    public List<City> cityList() {

        log.debug("cityList endpoint called");

        List<City> cities = cityService.getAllCities();
        return cities;

    }

    @GetMapping("/cityListObject")
    public CityListDTO cityListObject() {

        log.debug("cityListObject endpoint called");

        CityListDTO cityListDTO = new CityListDTO();
        cityListDTO.setCities(cityService.getAllCities());
        return cityListDTO;

    }

}
