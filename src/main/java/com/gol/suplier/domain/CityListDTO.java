package com.gol.suplier.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CityListDTO {

    List<City> cities;
}
