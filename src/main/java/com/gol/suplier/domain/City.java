package com.gol.suplier.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Getter
@Setter
public class City {

    private String code;
    private String name;
    private BigInteger residentsNumber;

    public City(String code, String name, BigInteger residentsNumber) {
        this.code = code;
        this.name = name;
        this.residentsNumber = residentsNumber;
    }
}
